import { openSync } from 'fs'

(() => {
  try {
    const path = './store.json'

    openSync(path, 'a')
  } catch (err) {
    console.error(err)
    throw err
  }
  console.info('Created store')
})()
