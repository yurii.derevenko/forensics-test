export const startHandler = async (context: any, db: any): Promise<string> => {
  const { min, max, sessionID } = context
  if (sessionID) return Promise.reject('Already started, please type ".reset" command for new round')
  const { uuid, maxAttempts } = await db.createRandomNumber({ min, max })

  console.log('\x1b[36m%s\x1b[0m', `
    Try to guess number in range between ${min} and ${max},
    you have ${maxAttempts} attempts
  `)

  return uuid
}
