# forensics-test

It is CLI app for guessing random number

## requirements
1) [nodejs](https://nodejs.org/en/)

## prestart

```bash
npm i
```

## start

```bash
npm start
```

## commands

1. create new random number
```bash
.start
```

2. try to guess number
```bash
.try $number
```

3. reset
```bash
.reset
```

4. exit
```bash
.exit
```

