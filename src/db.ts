import { appendFile, readFile, writeFile } from 'fs'
import { promisify } from 'util'
import * as uuid from 'uuid'

const pAppend = promisify(appendFile)
const pReadFile = promisify(readFile)
const pWriteFile = promisify(writeFile)

export class Database {
  path: string
  constructor (path: string) {
    this.path = path
  }

  public async createRandomNumber ({ min, max }: { min: number, max: number }): Promise<any> {
    const data = {
      uuid: uuid.v4(),
      attempts: 0,
      maxAttempts: 3,
      min,
      max
    }

    const appendData = JSON.stringify({ ...data, number: this.getRandomInt(min, max) }) + '\n'

    await pAppend(this.path, appendData)

    return data
  }

  private getRandomInt (min: number, max: number) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  public async getByIdAndTest (uuid: string, value: number): Promise<any> {
    const data = await pReadFile(this.path).then(res => res.toString())

    const jsonData = data.split('\n')
      .filter(d => Boolean(d.length))
      .map(d => JSON.parse(d))

    const message: any = {}

    jsonData.forEach(d => {
      if (d.uuid === uuid) {
        d.attempts = d.attempts + 1
        message.correct = false
        message.attemptsLeft = d.maxAttempts - d.attempts
        message.range = `${d.min}, ${d.max}`
        if (d.attempts > d.maxAttempts) {
          message.error = 'Max attempts exceded'
          message.attemptsLeft = 0
        }
        if (value === d.number) {
          message.correct = true
        }
      }
    })

    const newData = jsonData.map(d => JSON.stringify(d)).join('\n')

    await this.overWriteStore(newData + '\n')

    return message
  }

  private async overWriteStore (data: string): Promise<any> {
    await pWriteFile(this.path, data)
  }

}
