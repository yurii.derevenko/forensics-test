export const tryHandler = async (context: any, n: number, db: any): Promise<string> => {
  const { min, max, sessionID } = context

  if (!sessionID) return Promise.reject('You have not started, please type ".start"')
  if (!Number.isSafeInteger(n)) return Promise.reject('Should be integer')
  if (n > max || n < min) return Promise.reject(`Should be in range ${min} - ${max}`)

  const { error, correct, attemptsLeft } = await db.getByIdAndTest(sessionID, n)

  if (error) {
    return Promise.reject(error)
  }

  if (!correct) {
    return `Incorrect, ${attemptsLeft} attempts left`
  }

  if (correct) {
    return `Congrats, you guess number. Type ".reset" for new round`
  }
}
