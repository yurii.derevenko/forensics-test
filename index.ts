import * as repl from 'repl'

import { Database } from './src/db'
import { tryHandler, startHandler } from './src/handlers'

const db = new Database('./store.json')

function initializeContext (context) {
  context.max = 10
  context.min = 0
  context.sessionID = null
}

const replOptions: repl.ReplOptions = { prompt: '> ', useColors: true }
const replServer = repl.start(replOptions)

initializeContext(replServer.context)

console.log('\x1b[36m%s\x1b[0m', 'Greetings in number guessing app')

replServer.on('exit', () => {
  console.log('\x1b[33m%s\x1b[0m', 'Received "exit" event from repl!')
  process.exit()
})

replServer.defineCommand('reset', {
  help: 'Reset repl',
  action () {
    initializeContext(this.context)
    this.displayPrompt()
  }
})

replServer.defineCommand('start', {
  help: 'Type "start" to run number quiz',
  async action () {
    this.clearBufferedCommand()
    try {
      const uuid = await startHandler(this.context, db)
      this.context.sessionID = uuid
    } catch (ex) {
      console.log('\x1b[41m%s\x1b[0m', ex)
    }

    this.displayPrompt()
  }
})

replServer.defineCommand('try', {
  help: 'Type "try" to test your number',
  async action (number) {
    const n = Number(number)

    try {
      const message = await tryHandler(this.context, n, db)
      console.log('\x1b[36m%s\x1b[0m', message)
    } catch (ex) {
      console.log('\x1b[41m%s\x1b[0m', ex)
      console.log('\x1b[36m%s\x1b[0m', 'Type ".reset" for new round')
    }

    this.displayPrompt()
  }
})
